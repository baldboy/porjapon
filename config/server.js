module.exports = ({ env }) => ({
  host: env('HOST', '5.196.4.5'),
  port: env.int('PORT', 1337),
});
